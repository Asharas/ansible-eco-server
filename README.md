Ansible Eco Server
=========  

⚠ Documention may lack details, do not hesitate to open an issue or merge request if need be  

This role aims at deploying a self-hosted [Eco](https://play.eco/) server.  
It supports almost all of Eco configuration and some mods, see list below.  

Supported mods:
  - [EM Framework](https://eco.mod.io/elixr-mods-em-framework) (Dependency for DiscordLink)
  - [DiscordLink](https://github.com/Eco-DiscordLink/EcoDiscordPlugin)
  - [Big Shovel](https://eco.mod.io/big-shovel)
  - [Cost Calculator](https://eco.mod.io/cost-calculator) (disabled, mod broken)
  - [Chronicler](https://mod.io/g/eco/m/chronicler)

Some previously supported mods have been removed as maintenance turns out to be too much time consuming because of mod changes and brokenness. You can still install them manually.

If you like my work, please consider buying me a beer at [BuyMeACoffee.com](https://www.buymeacoffee.com/asharas)  

Requirements
------------


Role Variables
--------------  
Variables are split into two directories with three files each:  
- Vars located in [defaults/main/](defaults/main/) are what you most probably want to override.
  - [defaults/main/main.yml](#defaultsmainmainyml)   
  - [defaults/main/eco.yml](#defaultsmainecoyml)  
  - [defaults/main/discordlink.yml](#defaultsmaindiscordlinkyml)  


- Vars located in [vars/main/](vars/main/) are role related functional vars like mods urls, presets, etc. Most admins won't need to override these.

#### [defaults/main/main.yml](defaults/main/main.yml)  
Role specific variables.

| Variable | Description | Default |
| --- | --- | --- |
| eco_fqdn | Eco server Fully Qualified Domain Name | `''` |
| eco_iptables | Set iptables rules | `false` |
| eco_rp | Enable nginx as reverse proxy | `false` |
| eco_rp_tls | Enable nginx tls config | `false` |
| eco_rp_tls_selfsigned | Generate a self-signed X509 certificate | `false` |
| eco_rp_certbot | Enable use of [ansible-role-certbot](https://galaxy.ansible.com/geerlingguy/certbot), see [Dependencies](#Dependencies) | `false` |
| eco_rp_certbot_email | Let's Encrypt account email | `"admin@example.com"` |
| eco_rp_discord_redirect | Add a redirection `/discord` to Discord server URL | `false` |
| eco_skip_restart | Do not restart the server after playing the role even if changes occur | `false` |  
| eco_template_conf | Template configuration | `true` |
| eco_version | Specify branch to install | `default` |
| eco_em_framework_enable | Enable EM Framework mod | `false` |
| eco_dl_enable | Enable DiscordLink mod | `false` |
| eco_bigshovel_enable | Enable Big Shovel mod | `false` |
| eco_chronicler_enable | Enable Chronicler | `false` |
<!-- | eco_costcalc_enable | Enable Cost Calculator mod | `false` |   -->

#### [defaults/main/eco.yml](defaults/main/eco.yml)
Game specific variables.  
Those are too numerous to detail and doing so would be redundant. Take a look at the [defaults/main/eco.yml](defaults/main/eco.yml) file and [Eco server configuration](https://wiki.play.eco/en/Server_Configuration).  

#### [defaults/main/discordlink.yml](defaults/discordlink.yml)  
DiscordLink mod related variables, take a look at [DiscordLink Configuration Guide](https://github.com/Eco-DiscordLink/EcoDiscordPlugin/blob/develop/ConfigurationNoGUI.md).  

Dependencies
------------
[Jeff Geerling](https://www.jeffgeerling.com/)'s [certbot](https://galaxy.ansible.com/geerlingguy/certbot).  
Certbot is enabled by setting `eco_rp_certbot: true` and needs `eco_fqdn` and `eco_rp_certbot_email` to be set for the certificate to be generated.  
You'll need a valid domain name for the certificate to be generated. Getting one is beyond the scope of this document.  

Install by running the following command to get the required roles:  
```
ansible-galaxy install -r requirements.yml
```  

```yaml
---
- hosts: eco
  gather_facts: yes
  become: yes

  vars:
    eco_fqdn: eco.example.com
    eco_rp_certbot_email: admin@example.com
    # Set this var to true to request a staging certificate
    certbot_testmode: false

  roles:
    - role: eco_server
```

Example Playbook
----------------
#### Bare minimum
```yaml
---
- hosts: eco
  gather_facts: yes
  become: yes

  roles:
    - role: eco_server
```  

#### Custom difficulty  
To set your own difficulty you need to override the `eco_difficulty_presets` variable entirely.  

```yaml  
---
- hosts: eco
  gather_facts: yes
  become: yes

  vars:
    eco_difficulty: "custom"
    eco_specialty_refund_percent: 0.5
    eco_stacksize_modifier: 1.0
    eco_weight_modifier: 1.0
    eco_fuel_modifier: 1.0
    eco_growth_rate_mult: 1.0
    eco_conrange_modifier: 1.0
    eco_difficulty_presets:
      # eco_difficulty_presets.custom does not exists by default, it needs to be fully set in order to be used
      custom:
        name: "Custom"
        endgame_cost: "Normal"
        diff_mods:
          craft_resource_mod: 0.5
          craft_time_mod: 0.5
          player_xp_per_spec_xp: 0.0
          skill_gain_multiplier: 5
          spec_cost_multiplier: 0.1
          spec_xp_per_level: 25.0

  roles:
    - role: eco_server
```  
Same goes for DiscordLink variables like `eco_dl_chat` and similar dictionary variables.  

Notes
-----

#### Known problems  
- admin/white/black/mute lists and spawn coordinates in `Users.eco` will only be set from Ansible variables if file doesn't exist, else the file values will be preserved. This unfortunately means you can only change them through ingame admin actions.

#### Idempotency
This role isn't perfectly idempotent: because the game doesn't leave an empty trailing line in config files whereas most editors (and mine) do, some configuration files are templated again although the values haven't changed and trigger a game restart.    
Another difficulty that arose is that DiscordLink needs the `ID` value from `Configs/Network.eco` but this variable is only set after starting the server once. A solution might be to generate the `ID` and `Passport` UUIDs if they don't exist in `Configs/Network.eco`.  

#### Upcoming or considered features  

- Local `mods` directory for manual mods install
- NidToolBox mods
- Open to suggestions

License
-------

 <p xmlns:cc="http://creativecommons.org/ns#" xmlns:dct="http://purl.org/dc/terms/"><a property="dct:title" rel="cc:attributionURL" href="https://gitlab.com/Asharas/ansible-eco-server">Ansible Eco Server</a> by <a rel="cc:attributionURL dct:creator" property="cc:attributionName" href="https://gitlab.com/Asharas">Asharas</a> is licensed under <a href="http://creativecommons.org/licenses/by-nc-sa/4.0/?ref=chooser-v1" target="_blank" rel="license noopener noreferrer" style="display:inline-block;">CC BY-NC-SA 4.0<img style="height:22px!important;margin-left:3px;vertical-align:text-bottom;" src="https://mirrors.creativecommons.org/presskit/icons/cc.svg?ref=chooser-v1"><img style="height:22px!important;margin-left:3px;vertical-align:text-bottom;" src="https://mirrors.creativecommons.org/presskit/icons/by.svg?ref=chooser-v1"><img style="height:22px!important;margin-left:3px;vertical-align:text-bottom;" src="https://mirrors.creativecommons.org/presskit/icons/nc.svg?ref=chooser-v1"><img style="height:22px!important;margin-left:3px;vertical-align:text-bottom;" src="https://mirrors.creativecommons.org/presskit/icons/sa.svg?ref=chooser-v1"></a></p>

Author Information
------------------

[GitLab](https://gitlab.com/Asharas)  
[Mastodon](https://piaille.fr/@asharas)
[Twitter](https://twitter.com/AsharasInABox)
